# Introduction
This repository contains the code for the technical challenge issued to me 
by HighTechICT as part of their job interview.

The challenge involves a backend which models **mazes** intended for the
user to play using HTTP calls. There is a **Swagger OpenAPI** specification
for a auto-generated api client.

## Links
- [The challenge](https://maze.hightechict.nl/)
- [Swagger OpenAPI specification](https://maze.hightechict.nl/swagger/index.html)
- [Challenge rules](https://maze.hightechict.nl/dox/rules)

# Setup
## Configuration
### Application properties
Copy the `src/main/resources/application.properties.skeleton` to 
`src/main/resources/application.properties` and enter the api url
and api key to be used.

## Dependencies
This gradle setup requires my modified version of the generated Swagger api
client `swagger-java-client:1.0.1` to be in the local maven repository.
[Link to repository](https://gitlab.com/qa.ha/hightechict-maze-challenge-openapi-client)
Copy that repository, go to the root folder and run:
```
mvn clean install
```

Furthermore it utilises the *Maven Central* repository for *Spring*.

## Running the client
Navigate to the root folder of this repository and run:
```
./gradlew run
```

# Architecture
## Dependencies
### Swagger API Client
This program depends on a modified version of the auto-generated api client
by Swagger, which is a library that implements the HTTP requests available
on the back end, as wel als convenient model interfaces and objects specific
to the domain.

#### Elaboration on the modification
The automatically generated code contained a bug where it only accepted
HTTP responses in the form of `Content-Type: application/json`, which did not
comply with the actual API response `Content-Type: text/json`. Thus, it required
a simple edit to also accept this other content type. This edit was a simple
adjustment of the regex which was used as a condition. Luckily the parse code
continued to work out of the box with this adjustment.

### Spring
#### Dependency Injection
This program makes use of the Spring Inversion of Control (IoC) implementation
to handle it's dependency injection. Crucial to this implementation is the 
`AppConfig` class, which, along with the `@Component` annotated classes,
defines the available objects available for instantiation.

#### Application Properties
Furthermore, we leverage Spring's resource mechanisms to read a property file
where user specific secrets are stored in order to separate them from the code
base. A prime example for which this is used is the **api_key** used to connect
to the backend api.

## Key abstractions
### Client
The `Client` class is the main module which contains the `static main` function
used in order to bootstrap the program. It sets up the *Spring IoC container*
and intitialises and runs the `InteractionRunner`.

### Interactions
Interactions in a way are the **view** or **controller** abstraction of the 
program, in the context of a `MVC` abstraction. The `InteractionRunner` is 
the main unit which initialises with a single interaction `MainMenu`. 
Interactions can optionally return a next interaction to run, creating a 
dynamic sequence of menu interactions to run before the program exits.

### Game engine
The `GameEngine` class provides functionality directly related to the backend.
It simply wraps core functions provided by the automatically generated 
OpenAPI library provided by HighTechICT. The choice to use this class was
to keep track of the backend state in order to provide a consistent and
reliable interface to the user. 

### Bot
The `Bot` class implements the algorithm used in order to solve mazes
automatically. It greedily discovers unvisited tiles and emits events
through a simple `EventManager` to trigger bookkeeping requirements
in order to solve the maze.

### Path
Paths are used by the `Bot` implementation in order to keep track of important
objectives (Collect and Exit tiles). They are implemented as a simple
stack of directions representing the path taken from that tile to the
current tile. They are managed by the `PathManager`, which in turn
is subscribed to the events published by the `EventManager`.

# Improvements
## Exception handling
The exception handling could be improved to be more concise and self
documenting
## Bot algorithm
The bot algorithm could make use of tag information on the tiles to be more
efficient.
