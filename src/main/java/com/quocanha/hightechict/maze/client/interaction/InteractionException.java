/**
 * Exception class for maze relates issues.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction;

public class InteractionException extends Exception {
  public InteractionException() {
    super();
  }

  public InteractionException(String message) {
    super(message);
  }
}
