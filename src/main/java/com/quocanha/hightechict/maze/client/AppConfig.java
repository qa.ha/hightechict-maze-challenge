/**
 * Spring Configuration class implementation.
 *
 * Defines settings and the beans for the IoC container managed by Spring.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
 */

package com.quocanha.hightechict.maze.client;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;

import io.swagger.client.ApiClient;
import io.swagger.client.api.MazeApi;
import io.swagger.client.api.MazesApi;
import io.swagger.client.api.PlayerApi;

@Configuration
@ComponentScan(basePackages = "com.quocanha.hightechict.maze.client")
@PropertySource("classpath:application.properties")
public class AppConfig {

  @Bean
  ApiClient apiClient(@Value("${maze.apiUrl}") String apiUrl) {
    ApiClient api = io.swagger.client.Configuration.getDefaultApiClient();
    api.setBasePath(apiUrl);
    // Reset the singleton instance with the one including the base path.
    io.swagger.client.Configuration.setDefaultApiClient(api);

    return api;
  }

  @Bean("playerApi")
  @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
  PlayerApi playerApi() {
    return new PlayerApi();
  }

  @Bean("mazeApi")
  @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
  MazeApi mazeApi() {
    return new MazeApi();
  }

  @Bean("mazesApi")
  @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
  MazesApi mazesApi() {
    return new MazesApi();
  }

  /**
   * A global scanner bean.
   *
   * Implemented this way so that it is a singleton object which we can 
   * retrieve in order to properly close the stream.
   * @return scanner
   */
  @Bean
  @Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
  Scanner scanner() {
    return new Scanner(System.in);
  }
}
