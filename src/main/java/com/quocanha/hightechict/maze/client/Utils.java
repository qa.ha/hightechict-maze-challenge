/**
 * Utility class mainly for outputting text.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client;

public class Utils {
  public final static String PREFIX_INFO = "#";
  public final static String PREFIX_WARNING = "!";
  public final static String PREFIX_INPUT = ">";

  public static void printInfo(Object obj) {
    Utils.println(Utils.PREFIX_INFO + " " + obj);
  }
  public static void printWarning(Object obj) {
    Utils.println(Utils.PREFIX_WARNING + " " + obj);
  }
  public static void printInput(Object obj) {
    Utils.print(Utils.PREFIX_INPUT + " " + obj);
  }

  public static void println(Object obj) {
    System.out.println(obj);
  }

  public static void println() {
    System.out.println();
  }

  public static void print(Object obj) {
    System.out.print(obj);
  }
}
