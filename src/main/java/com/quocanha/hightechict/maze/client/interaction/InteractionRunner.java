/**
 * The interaction runner processes a dynamic sequence of interactions.
 *
 * Implemented by means of a stack. Interactions can optionally return
 * a new interaction, which is pushed on top of the stack.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
 */
package com.quocanha.hightechict.maze.client.interaction;

import java.util.Optional;
import java.util.Stack;

import org.springframework.stereotype.Component;

@Component
public class InteractionRunner {
  Stack<Interaction> queue = new Stack<Interaction>();

  /**
   * Start the sequence of interactions.
   * @return
  **/
  public void start() {
    while(!this.queue.isEmpty()) {
      Interaction interaction = this.queue.pop();

      try {
        Optional<Interaction> after = interaction.run();
        if(after.isPresent()) {
          this.queue.push(after.get());
        }
      } catch(InteractionException e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Add an interaction to the queue.
   * @return
  **/
  public void add(Interaction interaction) {
    this.queue.push(interaction);
  }
}
