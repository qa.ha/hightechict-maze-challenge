/**
 * Interaction for moving right while playing manually.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import org.springframework.stereotype.Component;

import io.swagger.client.model.MoveAction.DirectionEnum;

@Component
public final class MoveRightInteraction extends MoveInteraction {
  public DirectionEnum getDirection() {
    return DirectionEnum.RIGHT;
  }
}
