/**
 * Abstract interaction base class to reduce boilerplating.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.InputMismatchException;
import java.util.Scanner;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.engine.GameEngine;
import com.quocanha.hightechict.maze.client.interaction.Interaction;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public abstract class InteractionImpl implements Interaction, ApplicationContextAware {
  protected ApplicationContext appContext;

  @Autowired
  protected Scanner scanner;

  @Autowired
  protected GameEngine engine;

  /**
   * Get number input from System.in (standard input).
   * Keeps asking until a valid input has been found.
   * @return choice
  **/
  protected int getNumberChoice(int lowerLimit, int upperLimit) {
    while(true) {
      try {
        int choice = this.scanner.nextInt(); // Throws InputMismatchException
        this.scanner.nextLine(); // Consume dangling \n
        if(choice >= lowerLimit && choice <= upperLimit) {
          return choice;
        } else {
          Utils.printInput("Please enter a number in the range of " + lowerLimit + " to " + upperLimit + ".");
        }
      } catch (InputMismatchException e) { 
        if(this.scanner.hasNextLine()) // Consume dangling \n
          this.scanner.nextLine();
        Utils.printWarning("Please enter a number!");
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
  }

  /**
   * Get long input from System.in (standard input).
   * Keeps asking until a valid input has been found.
   * @return choice
  **/
  protected Long getLongChoice() {
    while(true) {
      try {
        Long choice = this.scanner.nextLong(); // Throws InputMismatchException
        this.scanner.nextLine(); // Consume dangling \n
        return choice;
      } catch (InputMismatchException e) { 
        if(this.scanner.hasNextLine()) // Consume dangling \n
          this.scanner.nextLine();
        Utils.printWarning("Please enter a number!");
      } catch(Exception e) {
        e.printStackTrace();
      }
    }
  }

  public void setApplicationContext(ApplicationContext context) throws BeansException {
    this.appContext = context;
  }
}
