/**
 * Interface for an event listener.
**/
package com.quocanha.hightechict.maze.client.bot;

import com.quocanha.hightechict.maze.client.bot.EventManager.Event;

public interface EventListener {
  public void doAction(Event event);
}
