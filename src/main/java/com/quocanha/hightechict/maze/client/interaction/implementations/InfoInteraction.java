/**
 * Interaction for displaying information.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

import org.springframework.stereotype.Component;

import io.swagger.client.model.PlayerInfo;

@Component
public class InfoInteraction extends InteractionImpl {
  public Optional<Interaction> run() throws InteractionException {
    PlayerInfo info = this.engine.getPlayerInfo();
    if(info == null) {
      throw new InteractionException();
    }

    Utils.println(info);
    return Optional.of(this.appContext.getBean(MainMenu.class));
  }
}
