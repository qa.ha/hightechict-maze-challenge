/**
 * Interaction for quitting the client.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

public class QuitInteraction extends InteractionImpl {

  public Optional<Interaction> run() throws InteractionException {
    Utils.println("Thanks for playing!");
    return Optional.empty();
  }
}
