/**
 * Standard event manager implementation.
 *
 * Keeps a list of listeners which are invoked when an event is emitted.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.bot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import io.swagger.client.model.MoveAction.DirectionEnum;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class EventManager {

  @Component
  public interface Event {}

  @Component
  public static class FoundCollectEvent implements Event {}
  @Component
  public static class FoundExitEvent implements Event {}
  @Component
  public static class FoundStartEvent implements Event {}

  @Component
  public static class MoveEvent implements Event{
    private DirectionEnum direction;

    public void setDirection(DirectionEnum dir) {
      this.direction = dir;
    }

    public DirectionEnum getDirection() {
      return this.direction;
    }
  }

  private List<EventListener> listeners = new ArrayList<>();

  public EventManager() {}

  public void register(EventListener listener) {
    this.listeners.add(listener);
  }

  public void emit(Event event) {
    for(EventListener listener: this.listeners) {
      listener.doAction(event);
    }
  }
}
