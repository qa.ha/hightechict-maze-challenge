/**
 * Engine that drives the game (state).
 *
 * A wrapper around the generated API in order to keep track of the game 
 * state without having to request all information every time besides 
 * allowing for easy changes.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
 */
package com.quocanha.hightechict.maze.client.engine;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.api.MazeApi;
import io.swagger.client.api.MazesApi;
import io.swagger.client.api.PlayerApi;
import io.swagger.client.auth.ApiKeyAuth;
import io.swagger.client.model.MazeInfo;
import io.swagger.client.model.PlayerInfo;
import io.swagger.client.model.PossibleActionsAndCurrentScore;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class GameEngine {

  @Autowired
  private ApiClient apiClient;

  @Autowired
  private PlayerApi playerApi;

  @Autowired
  private MazeApi mazeApi;

  @Autowired
  private MazesApi mazesApi;

  @Value("${maze.apiKey}")
  private String apiKey;

  public boolean initialised = false;

  private PlayerInfo playerInfo;

  private List<MazeInfo> allMazes;

  private PossibleActionsAndCurrentScore possibleActions;

  /**
   * Initialise the session by setting the api key.
   *
   * @return
   */
  public void initialise() {
    ApiKeyAuth userToken = (ApiKeyAuth) this.apiClient.getAuthentication("User token");
    userToken.setApiKey(this.apiKey);
    
    this.requestPlayerInfo();

    this.initialised = true;
  }

  /**
   * Request player information from the backend.
   *
   * @return
  **/
  public void requestPlayerInfo() {
    try {
      this.playerInfo = this.playerApi.get();
      return;
    } catch(ApiException e) {
      if(e.getCode() == 404) {
        // Not registered
      } else {
        e.printStackTrace();
      }
      this.playerInfo = null;
      return;
    }
  }

  /**
   * Request possible actions possible on the current location from the backend.
   *
   * @return
  **/
  public void requestActions() throws ApiException {
    this.possibleActions = this.mazeApi.possibleActions();
  }

  /**
   * Register a player name on the backend.
   *
   * @return
  **/
  public void requestRegister(String name) throws ApiException {
    this.playerApi.register(name);
    this.requestPlayerInfo();
  }

  /**
   * Forget the player name and progress on the backend.
   *
   * @return
  **/
  public void requestForget() throws ApiException {
    this.playerApi.forget();
    this.playerInfo = null;
  }

  /**
   * Request a collect action on the backend while in a maze.
   *
   * @return updated tile information
  **/
  public PossibleActionsAndCurrentScore requestCollect() throws ApiException {
      this.possibleActions = this.mazeApi.collectScore();
      return this.possibleActions;
  }

  /**
   * Request the information of all mazes.
   *
   * Caches the response since it never changes on the backend.
   *
   * @return all mazes
  **/
  public List<MazeInfo> requestAllMazes() throws ApiException {
    if(this.allMazes == null ) {
      this.allMazes = this.mazesApi.all();
    }
    return this.allMazes;
  }

  /**
   * Get maze information of a single maze.
   *
   * @return maze info
  **/
  public Optional<MazeInfo> getMazeInfo(String name) throws ApiException {
    return this.requestAllMazes().stream()
      .filter(maze -> maze.getName().equals(name))
      .findFirst();
  }

  /**
   * Attempt to enter a maze.
   *
   * @return
  **/
  public void requestEnter(String name) throws ApiException {
    this.possibleActions = this.mazesApi.enter(name);
    this.requestPlayerInfo();
  }

  /**
   * Attempt to exit a maze.
   *
   * @return
  **/
  public void requestExit() throws ApiException {
    this.mazeApi.exitMaze();
    this.requestPlayerInfo();
    this.possibleActions = null;
  }

  /**
   * Attempt to move into a given direction.
   *
   * @return PossibleActionsAndCurrentScore
  **/
  public PossibleActionsAndCurrentScore requestMove(String direction) throws ApiException {
    this.possibleActions = this.mazeApi.move(direction);
    return this.possibleActions;
  }

  /**
   * Tag the current tile with a given value.
   *
   * @return 
  **/
  public void requestTag(Long tag) throws ApiException {
    this.possibleActions = this.mazeApi.tag(tag);
  }

  /**
   * Has the player registered?
   *
   * @return isRegistered
  **/
  public boolean isRegistered() {
    assert this.initialised = true;

    if(this.playerInfo != null) {
      return true;
    }
    return false;
  }

  /**
   * Is the player inside a maze?
   *
   * @return isInMaze
  **/
  public boolean isInMaze() {
    assert this.initialised = true;

    if(this.isRegistered()) {
      if(this.playerInfo.isIsInMaze()) {
        return true;
      }
    }
    return false;
  }

  /**
   * Retrieve the player information.
   *
   * @return PlayerInfo
  **/
  public PlayerInfo getPlayerInfo() {
    return this.playerInfo;
  }

  /**
   * Get current tile information.
   *
   * @return PossibleActionsAndCurrentScore
  **/
  public PossibleActionsAndCurrentScore getCurrentTile() {
    return this.possibleActions;
  }
}
