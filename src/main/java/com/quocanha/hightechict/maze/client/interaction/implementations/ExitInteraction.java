/**
 * Exit interaction while playing manually.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;

@Component
public class ExitInteraction extends InteractionImpl {
  public Optional<Interaction> run() throws InteractionException {
    try {
      this.engine.requestExit();
    } catch(ApiException e) {
      if(e.getCode() == 403) {
        Utils.printWarning("Not an exit tile.");
      } else if(e.getCode() == 412) {
        Utils.printWarning("You haven't entered a maze yet, so you cannot tag a tile.");
      } else {
        e.printStackTrace();
      }
    }

    Interaction mainMenu = this.appContext.getBean(MainMenu.class);
    return Optional.of(mainMenu);
  }
}
