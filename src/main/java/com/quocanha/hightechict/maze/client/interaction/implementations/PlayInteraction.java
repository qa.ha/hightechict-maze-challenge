/**
 * Play interaction for when inside a maze.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

import org.springframework.stereotype.Component;

import io.swagger.client.model.MoveAction;
import io.swagger.client.model.PossibleActionsAndCurrentScore;

@Component
public final class PlayInteraction extends InteractionImpl {

  public Optional<Interaction> run() throws InteractionException {
    try {
      LinkedHashMap<String, Interaction> options = this.getOptions();

      Utils.println("**** Play action ****");
      this.showTileInfo();
      Utils.println("*********************");
      this.showOptions(options);
      Utils.printInput("Choice: ");
      int choice = this.getNumberChoice(1, options.size());
      Utils.println("*********************");
      Utils.println();

      // Retrieve and return the chosen interaction
      Interaction interaction = this.getInteractionByIndex(choice, options);
      if(interaction != null) {
        return Optional.of(interaction);
      } else {
        return Optional.empty();
      }

    } catch (Exception e) {
      e.printStackTrace();
      return Optional.of(this.appContext.getBean(MainMenu.class));
    }
  }

  /**
   * Show characteristics of the current tile.
  **/
  private void showTileInfo() {
    PossibleActionsAndCurrentScore tile = this.engine.getCurrentTile();

    Utils.println("* Score: " + " bag(" + tile.getCurrentScoreInBag() + ")" 
        + " hand(" + tile.getCurrentScoreInHand() + ")");

    String tileInfo = "tag(" + tile.getTagOnCurrentTile() + ")";
    if(tile.isCanExitMazeHere()) {
      tileInfo += " exit";
    }
    if(tile.isCanCollectScoreHere()) {
      tileInfo += " collect";
    }

    Utils.println("* Current tile: " + tileInfo);

  }

  /**
   * Generates the options list based on player state.
   * @return options
   */
  private LinkedHashMap<String, Interaction> getOptions() {
    LinkedHashMap<String, Interaction> options = new LinkedHashMap<String, Interaction>();

    PossibleActionsAndCurrentScore actionsAndScore = this.engine.getCurrentTile();

    List<MoveAction> moveActions = actionsAndScore.getPossibleMoveActions();
    for(MoveAction action: moveActions) {

      String info = "";
      if(action.isHasBeenVisited()) {
        info += "visited ";
      } else {
        info += "not_visited ";
      }

      if(action.isIsStart()) {
        info += "isStart ";
      }
      if(action.isAllowsExit()) {
        info += "isExit ";
      }
      if(action.isAllowsScoreCollection()) {
        info += "canCollect ";
      }

      if(action.getTagOnTile() != null) {
        info += "tag(" + action.getTagOnTile() + ") ";
      }

      switch(action.getDirection().toString()) {
        case "Up":
          options.put("Move " + action.getDirection() 
              + "(" + action.getRewardOnDestination() + ")" + " " + info,
              this.appContext.getBean(MoveUpInteraction.class));
          break;
        case "Right":
          options.put("Move " + action.getDirection() 
              + "(" + action.getRewardOnDestination() + ")" + " " + info,
              this.appContext.getBean(MoveRightInteraction.class));
          break;
        case "Down":
          options.put("Move " + action.getDirection() 
              + "(" + action.getRewardOnDestination() + ")" + " " + info,
              this.appContext.getBean(MoveDownInteraction.class));
          break;
        case "Left":
          options.put("Move " + action.getDirection() 
              + "(" + action.getRewardOnDestination() + ")" + " " + info,
              this.appContext.getBean(MoveLeftInteraction.class));
          break;
      }
    }
    if(actionsAndScore.isCanCollectScoreHere()) {
      options.put("Collect", this.appContext.getBean(CollectInteraction.class));
    }
    options.put("Tag", this.appContext.getBean(TagInteraction.class));
    if(actionsAndScore.isCanExitMazeHere()) {
      options.put("Exit", this.appContext.getBean(ExitInteraction.class));
    }

    options.put("Main menu", this.appContext.getBean(MainMenu.class));
    return options;
  }

  /**
   * Print the option labels with their index.
   *
   * The index relies on the static ordering of a LinkedHashMap.
   */
  public void showOptions(LinkedHashMap<String, Interaction> options) {
    int counter = 1;
    for(String label: options.keySet()) {
      Utils.println("* " + counter + ". " + label);
      counter++;
    }
  }

  /**
   * Retrieve interaction by index.
   *
   * The index paramater is the index of the order in which the options list 
   * has been created, leveraging the ordered items of a LinkedHashMap.
   * @return interaction
   */
  private Interaction getInteractionByIndex(int index, 
      LinkedHashMap<String, Interaction> options) 
    throws InteractionException
  {
    int counter = 1;

    // for(Interaction interaction: options.values()) {
    for(Map.Entry<String, Interaction> entry: options.entrySet()) {
      if(counter == index) {
        return entry.getValue();
      }
      counter++;
    }

    throw new InteractionException();
  }
}
