/**
 * Client.
 *
 * Contains the static main function and bootstraps the engine and interactions.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
 */

package com.quocanha.hightechict.maze.client;

import com.quocanha.hightechict.maze.client.interaction.implementations.MainMenu;
import com.quocanha.hightechict.maze.client.engine.GameEngine;
import com.quocanha.hightechict.maze.client.interaction.InteractionRunner;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Client implements ApplicationContextAware {
  private ApplicationContext applicationContext;

  @Autowired
  private GameEngine engine;

  @Value("${maze.apiKey}")
  private String apiKey;

  public static void main(String[] args) {
    ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
    try {
      Client client = context.getBean(Client.class);
      client.run();
    } finally {
      ((AnnotationConfigApplicationContext) context).close();
    }
  }

  /**
   * Run the client.
   * @return
   */
  public void run() {
    this.engine.initialise();

    InteractionRunner runner = new InteractionRunner();

    runner.add(this.applicationContext.getBean(MainMenu.class));
    runner.start();
  }

  public void setApplicationContext(ApplicationContext context) throws BeansException {
    this.applicationContext = context;
  }
}
