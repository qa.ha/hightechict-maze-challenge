/**
 *
 * A class that automatically clears a maze.
 *
 * The algorithm greedily explores unvisited neighbours while keeping track of 
 * the path back to Start. Upon discovery of an Exit or Collect tile, it tracks
 * these paths as well.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.bot;

import java.util.List;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.bot.EventManager.FoundCollectEvent;
import com.quocanha.hightechict.maze.client.bot.EventManager.FoundExitEvent;
import com.quocanha.hightechict.maze.client.bot.EventManager.FoundStartEvent;
import com.quocanha.hightechict.maze.client.bot.EventManager.MoveEvent;
import com.quocanha.hightechict.maze.client.bot.PathManager.Path;
import com.quocanha.hightechict.maze.client.bot.PathManager.PathType;
import com.quocanha.hightechict.maze.client.engine.GameEngine;
import com.quocanha.hightechict.maze.client.MazeException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;
import io.swagger.client.model.MazeInfo;
import io.swagger.client.model.MoveAction;
import io.swagger.client.model.PossibleActionsAndCurrentScore;
import io.swagger.client.model.MoveAction.DirectionEnum;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Bot {

  @Autowired
  private GameEngine engine;

  @Autowired
  private EventManager eventManager;

  @Autowired
  private PathManager pathManager;

  private int processedTiles = 0;

  private MazeInfo mazeInfo;

  /**
   * Play the maze.
   *
   * 1) Enters the provided maze
   * 2) Greedily discovers all tiles, collecting rewards.
   *    2.1) Upon discovery of Collect or Exit tiles, tracks the steps taken from
   *         that point on to any new locations we go.
   * 3) Travels towards collection point and collects.
   * 4) Travels towards exit point and exits.
   * @return
  **/
  public void play(String maze) {
    // this.reset();
    try {

      this.engine.requestEnter(maze);
      this.mazeInfo = this.engine.getMazeInfo(maze).get();

      this.processStartTile();

      this.discover();

      Path collectPath = Path.reverse(this.pathManager.getNearestPath(PathType.COLLECT));
      this.traverse(collectPath);
      this.engine.requestCollect();

      Path exitPath = Path.reverse(this.pathManager.getNearestPath(PathType.EXIT));
      this.traverse(exitPath);
      this.engine.requestExit();

      this.engine.requestPlayerInfo(); // Update player info
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Process the start tile since we don't iterate over it in our recursive 
   * function.
   *
   * @return
  **/
  public void processStartTile() {
    this.eventManager.emit(new FoundStartEvent());

    PossibleActionsAndCurrentScore current = this.engine.getCurrentTile();
    if(current.isCanCollectScoreHere()) {
      this.eventManager.emit(new FoundCollectEvent());
    }

    if(current.isCanExitMazeHere()) {
      this.eventManager.emit(new FoundExitEvent());
    }
    this.processedTiles++;
  }

  /**
   * Recursive function that iterates over all unvisited neighbours.
   *
   * Base case: if there are no unvisited neighbours, traverse back towards
   *   start using the back queue to look for unvisited directions.
   * Iterative case: if there are unvisited neighbours, move towards one and
   *   add the opposite move action to the back queue.
   *
   *  @return
  **/
  public void discover() {
    Path fromStart = this.pathManager.getNearestPath(PathType.START);
    MoveAction unvisited = this.getUnvisitedNeighour();
    if(unvisited != null) {
      // Discovering new branch / node, iterative case.
      try {
        // Move to new node
        this.engine.requestMove(unvisited.getDirection().toString());

        PossibleActionsAndCurrentScore newCurrentTile = this.engine.getCurrentTile();

        // Trigger events
        MoveEvent event = new MoveEvent();
        event.setDirection(unvisited.getDirection());
        this.eventManager.emit(event);

        if(newCurrentTile.isCanCollectScoreHere()) {
          this.eventManager.emit(new FoundCollectEvent());
        }

        if(newCurrentTile.isCanExitMazeHere()) {
          this.eventManager.emit(new FoundExitEvent());
        }

        this.processedTiles++;

        // Recurse
        this.discover();
      } catch(ApiException e) {
        e.printStackTrace();
      }
    } else if(!fromStart.getSteps().isEmpty() 
        && this.processedTiles < this.mazeInfo.getTotalTiles() ){
      // Branch cleared, move back, no need to add a back action since we're 
      // actually consuming it.
      // Base case, since we're not adding more iterations, even though we
      // do a recursive call.
      try {
        // Get back direction
        DirectionEnum lastStep = fromStart.getSteps().get(
            fromStart.getSteps().size() - 1);
        DirectionEnum backStep = Path.getOppositeDirection(lastStep);

        this.engine.requestMove(backStep.toString());

        // Trigger events
        MoveEvent event = new MoveEvent();
        event.setDirection(backStep);
        this.eventManager.emit(event);

        // Recurse
        this.discover();
      } catch(ApiException e) {
        e.printStackTrace();
      }
    } else {
      // Base case, all tiles have been discovered.
    }
  }

  /**
   * Walk down a path.
   *
   * Consumes the path in the process.
   * @return
   */
  public void traverse(Path path) throws MazeException {
    try {
      while(!path.getSteps().isEmpty()) {
        DirectionEnum step = path.getSteps().pop();
        this.engine.requestMove(step.toString());

        // Trigger event
        MoveEvent event = new MoveEvent();
        event.setDirection(step);
        this.eventManager.emit(event);
      }
    } catch(ApiException e) {
      e.printStackTrace();
    }
  }

  /**
   * Retrieve the action to go back.
   * @return backAction
  **/
  public MoveAction getBackAction(List<MoveAction> actions, MoveAction prev) throws MazeException {
    DirectionEnum backDirection = Path.getOppositeDirection(prev.getDirection());

    for(MoveAction possibleMove: actions) {
      if(possibleMove.getDirection().toString().equals(backDirection.toString())) {
        return possibleMove;
      }
    }
    throw new MazeException("Back action not found in possible move actions.");
  }

  /**
   * Get the first listed unvisited neighbour.
   * @return the neighbour or null when every neigbour has been visited.
  **/
  public MoveAction getUnvisitedNeighour() {
    PossibleActionsAndCurrentScore actions = this.engine.getCurrentTile();
    for(MoveAction action: actions.getPossibleMoveActions()) {
      if(!action.isHasBeenVisited()) {
        return action;
      }
    }
    return null;
  }

  /**
   * Print object.
   * @return
  **/
  public void print(Object obj) {
    Utils.printInfo(obj);
  }
}
