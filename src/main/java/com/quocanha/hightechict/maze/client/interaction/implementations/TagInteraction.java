/**
 * Interaction for tagging a tile while playing manually.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;

@Component
public class TagInteraction extends InteractionImpl {
  public Optional<Interaction> run() throws InteractionException {
    try {
      Utils.printInput("Please enter a tag value: ");
      Long tag = this.getLongChoice();

      this.engine.requestTag(tag);
      Utils.println();

    } catch(ApiException e) {
      if(e.getCode() == 412) {
        Utils.printWarning("You haven't entered a maze yet, so you cannot tag a tile.");
      } else {
        e.printStackTrace();
      }
    }

    Interaction playInteraction = this.appContext.getBean(PlayInteraction.class);
    return Optional.of(playInteraction);
  }
}
