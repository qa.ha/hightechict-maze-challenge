/**
 * Interaction for registering a player name.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;

@Component
public class RegisterInteraction extends InteractionImpl {

  public Optional<Interaction> run() throws InteractionException {
    Utils.printInput("Please enter a name to register yourself with: ");
    String name = this.scanner.nextLine();

    try {
      this.engine.requestRegister(name);

      Utils.printInfo("Registered succesfully!");
      Utils.println();

    } catch(ApiException e) {
      if(e.getCode() == 409) {
        Utils.printWarning(
            "You have already registered, " +
            "please forget the current on first before re-registering.");
      } else {
        e.printStackTrace();
      }
    }

    Interaction mainMenu = this.appContext.getBean(MainMenu.class);
    return Optional.of(mainMenu);
  }
}
