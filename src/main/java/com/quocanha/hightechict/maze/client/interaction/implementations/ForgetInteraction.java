/**
 * Interaction for forgetting the registration.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;

@Component
public class ForgetInteraction extends InteractionImpl {
  public Optional<Interaction> run() throws InteractionException {
    try {
      this.engine.requestForget();

      Utils.printInfo("Forgot registration!");
      Utils.println();
    } catch(ApiException e) {
      e.printStackTrace();
    }

    return Optional.of(this.appContext.getBean(MainMenu.class));
  }
}
