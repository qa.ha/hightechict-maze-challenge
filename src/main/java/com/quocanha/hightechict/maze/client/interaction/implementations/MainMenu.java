/**
 * Main menu interaction.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.engine.GameEngine;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;
import io.swagger.client.api.PlayerApi;

@Component
public class MainMenu extends InteractionImpl {

  @Autowired
  public PlayerApi playerApi;

  @Autowired
  public Scanner scanner;

  @Autowired
  private GameEngine engine;

  /**
   * Run the interaction.
   * @return
  **/
  @Override
  public Optional<Interaction> run() throws InteractionException {
    LinkedHashMap<String, Interaction> options = this.getOptions();

    Utils.println("**** Main menu ****");
    this.showOptions(options);
    Utils.println("*******************");
    Utils.printInput("Choice: ");
    int choice = this.getNumberChoice(1, options.size());
    Utils.println("*******************");

    try {
      Interaction interaction = this.getInteractionByIndex(choice, options);
      if(interaction != null) {
        return Optional.of(interaction);
      } else {
        return Optional.empty();
      }

    } catch (Exception e) {
      e.printStackTrace();
      return Optional.of(this.appContext.getBean(MainMenu.class));
    }
  }

  /**
   * Generates the options list based on player state.
   * @return options
   */
  private LinkedHashMap<String, Interaction> getOptions() {
    LinkedHashMap<String, Interaction> options = new LinkedHashMap<String, Interaction>();

    if(this.engine.isRegistered()) {
      options.put("Info", this.appContext.getBean(InfoInteraction.class));
      options.put("Forget", this.appContext.getBean(ForgetInteraction.class));
      if(!this.engine.isInMaze()) {
        options.put("Play", this.appContext.getBean(EnterInteraction.class));
        options.put("Autoplay", this.appContext.getBean(AutoPlayInteraction.class));
      } else {
        // Manually retrieve the possible actions,
        // since we are inside a maze
        try {
          this.engine.requestActions(); 
        } catch(ApiException e) {
          if(e.getCode() == 412) {
            // Not in maze
            // Impossible since we checked inMaze above.
            System.out.println("Impossible exception occurred.");
            e.printStackTrace();
          } else {
            e.printStackTrace();
          }
        }
        options.put("Resume", this.appContext.getBean(PlayInteraction.class));
      }
    } else {
      options.put("Register", this.appContext.getBean(RegisterInteraction.class));
    }
    options.put("Quit", null);

    return options;
  }

  /**
   * Print the option labels with their index.
   * The index relies on the static ordering of a LinkedHashMap.
   * @return
   */
  public void showOptions(LinkedHashMap<String, Interaction> options) {
    int counter = 1;
    for(String label: options.keySet()) {
      System.out.println("* " + counter + ". " + label);
      counter++;
    }
  }

  /**
   * Retrieve interaction by index.
   * The index paramater is the index of the order in which the options list 
   * has been created, leveraging the ordered items of a LinkedHashMap.
   * @return interaction
   */
  private Interaction getInteractionByIndex(int index, 
      LinkedHashMap<String, Interaction> options) 
    throws InteractionException
  {
    int counter = 1;

    // for(Interaction interaction: options.values()) {
    for(Map.Entry<String, Interaction> entry: options.entrySet()) {
      if(counter == index) {
        return entry.getValue();
      }
      counter++;
    }

    throw new InteractionException();
  }
}
