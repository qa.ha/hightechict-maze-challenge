/**
 * Exception class for maze related issues.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
 */
package com.quocanha.hightechict.maze.client;

public class MazeException extends Exception {
  public MazeException() {
    super();
  }

  public MazeException(String message) {
    super(message);
  }
}
