/**
 * The interaction interface defines a run method to be called by 
 * the interaction runner.
 *
 * See {@link InteractionRunner}
 *
 * @author Quoc An Ha (qa.ha@pm.me)
 */
package com.quocanha.hightechict.maze.client.interaction;

import java.util.Optional;

public interface Interaction {
  public Optional<Interaction> run() throws InteractionException;
}
