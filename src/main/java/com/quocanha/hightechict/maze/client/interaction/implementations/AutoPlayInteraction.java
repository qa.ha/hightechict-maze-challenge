/**
 * Interaction for setting up the bot.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.bot.Bot;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;
import com.quocanha.hightechict.maze.client.MazeException;

import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;
import io.swagger.client.model.MazeInfo;

@Component
public class AutoPlayInteraction extends InteractionImpl {

  public Optional<Interaction> run() throws InteractionException {
    Utils.printWarning(
        "Warning: Autoplay only works when you have not yet visited the maze!");

    try {
      List<String> mazes = this.promptMazeChoice();
      if(this.engine.isInMaze()) {
        Utils.printWarning("You can't use autoplay if you're already in a maze.");
      } else {
        for(String maze: mazes) {
          this.autoPlayMaze(maze);
        }
      }
    } catch (ApiException | MazeException e) {
      e.printStackTrace();
    }
    return Optional.of(this.appContext.getBean(MainMenu.class));
  }

  public void autoPlayMaze(String maze) {
    Utils.printInfo("Autoplaying " + maze);
    Bot bot = this.appContext.getBean(Bot.class);
    bot.play(maze);
  }

  /**
   * Prompt the user for maze selection.
   *
   * @return list of selected mazes
  **/
  public List<String> promptMazeChoice() throws ApiException, MazeException {
    List<MazeInfo> mazes = this.engine.requestAllMazes();

    // Print options
    int choice = -1;
    while(choice < 0) {
      this.printMazes(mazes);
      Utils.printInput("Please choose what maze to autoplay: ");

      try {
        // Get input
        int input = this.scanner.nextInt();

        if(input < 0 || input > mazes.size())
          throw new IllegalArgumentException();

        choice = input;
      } catch(InputMismatchException e) {
        Utils.printWarning("Please be sure to enter a number.");
      } catch(IllegalArgumentException e) {
        Utils.printWarning("Please enter a number between 0 and " +
            mazes.size() + ".");
      } finally {
        this.scanner.nextLine(); // Consume dangling newline
      }
    }

    if(choice == 0) {
      // Return all mazes
      return mazes.stream().map(maze -> maze.getName()).toList();
    }

    // Return single maze choice
    return new ArrayList<String>(
        Arrays.asList(this.getMazeNameByIndex(choice)));
  }

  public void printMazes(List<MazeInfo> mazes) {
    Utils.println("****** Mazes ******");
    Utils.println("* 0. All");
    for(int i = 0; i < mazes.size(); i++) {
      MazeInfo maze = mazes.get(i);
      Utils.println("* " + (i + 1) + ". " + 
          maze.getName() + 
          " - " + maze.getTotalTiles() + 
          " - " + maze.getPotentialReward());
    }
    Utils.println("*******************");
  }

  /**
   * Retrieve maze name.
   *
   * Relies on the consistent ordering of the list of mazes returned by the 
   * backend.
   *
   * @return name
  **/
  public String getMazeNameByIndex(int index) throws MazeException {
    int count = 0;
    try {
      for(MazeInfo maze : this.engine.requestAllMazes()) {
        count++;
        if(count == index) {
          return maze.getName();
        }
      }
    } catch(ApiException e) {
      e.printStackTrace();
    }
    throw new MazeException("Unknown maze index number");
  }
}
