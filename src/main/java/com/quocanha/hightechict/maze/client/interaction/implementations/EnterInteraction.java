/**
 * Interaction for manually entering a maze.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Optional;

import com.quocanha.hightechict.maze.client.Utils;
import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;
import com.quocanha.hightechict.maze.client.MazeException;

import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;
import io.swagger.client.model.MazeInfo;

@Component
public class EnterInteraction extends InteractionImpl {
  public Optional<Interaction> run() throws InteractionException {
    try {
      String name = this.promptMazeChoice();

      this.engine.requestEnter(name);

      Utils.printInfo("Entered maze '" + name + "'!");
    } catch(ApiException e) {
      switch(e.getCode()) {
        case 404:
          Utils.printWarning("The maze entered does not exist.");
          break;
        case 409:
          Utils.printWarning("You already are inside of a maze.");
          break;
        case 412:
          Utils.printWarning("You should first register.");
          break;
        default:
          e.printStackTrace();
      }
    } catch(MazeException e) {
      e.printStackTrace();
    }

    return Optional.of(this.appContext.getBean(PlayInteraction.class));
  }

  /**
   * Prompt the user for maze selection.
   */
  public String promptMazeChoice() throws ApiException, MazeException {
    List<MazeInfo> mazes = this.engine.requestAllMazes();

    // Print options
    int choice = -1;
    while(choice < 0) {
      this.printMazes(mazes);
      Utils.printInput("Please choose what maze to play: ");
      try {
        // Get input
        int input = this.scanner.nextInt();

        if(input < 0 || input > mazes.size())
          throw new IllegalArgumentException();

        choice = input;
      } catch(InputMismatchException e) {
        Utils.printWarning("Please be sure to enter a number.");
      } catch(IllegalArgumentException e) {
        Utils.printWarning("Please enter a number between 0 and " +
            mazes.size() + ".");
      } finally {
        this.scanner.nextLine(); // Consume dangling newline
      }
    }

    // Return single maze choice
    return this.getMazeNameByIndex(choice);
  }

  public void printMazes(List<MazeInfo> mazes) {
      Utils.println("****** Mazes ******");
      for(int i = 1; i <= mazes.size(); i++) {
        MazeInfo maze = mazes.get(i-1);
        Utils.println("* " + i + ". " + 
            maze.getName() + 
            " - " + maze.getTotalTiles() + 
            " - " + maze.getPotentialReward());
      }
      Utils.println("*******************");
  }

  /**
   * Retrieve maze name.
   *
   * Relies on the consistent ordering of the list of mazes returned by the 
   * backend.
   *
   * @return name
  **/
  public String getMazeNameByIndex(int index) throws MazeException {
    int count = 0;
    try {
      for(MazeInfo maze : this.engine.requestAllMazes()) {
        count++;
        if(count == index) {
          return maze.getName();
        }
      }
    } catch(ApiException e) {
      e.printStackTrace();
    }
    throw new MazeException("Unknown maze index number");
  }
}
