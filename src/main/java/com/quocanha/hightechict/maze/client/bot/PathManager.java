/**
 * The path manager keeps track of multiple tiles of interests.
 *
 * A path is represented by a sequence of directions in which was moved.
 *
 * The path manager listens to events on the bot through the {@link EventManager}
 * abstraction. Upon discovery of an EXIT or COLLECT tile new paths are tracked.
 * Upon movement of the player all paths are updates accordingly.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.bot;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import com.quocanha.hightechict.maze.client.bot.EventManager.Event;
import com.quocanha.hightechict.maze.client.bot.EventManager.FoundCollectEvent;
import com.quocanha.hightechict.maze.client.bot.EventManager.FoundExitEvent;
import com.quocanha.hightechict.maze.client.bot.EventManager.FoundStartEvent;
import com.quocanha.hightechict.maze.client.bot.EventManager.MoveEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import io.swagger.client.model.MoveAction.DirectionEnum;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PathManager implements EventListener {
  public enum PathType {
    START, COLLECT, EXIT
  }

  /**
   * A path represented as a sequence of move directions.
  **/
  @Component
  public static class Path {
    private Stack<DirectionEnum> steps = new Stack<>();

    public String toString() {
      return steps.toString();
    }

    /**
     * Reverse path by reversing the ordering and mirorring 
     * the step direction.
     *
     * @return reversed path
    **/
    public static Path reverse(Path path) {
      Path reversed = new Path();
      for(DirectionEnum step: path.steps) {
        reversed.steps.add(Path.getOppositeDirection(step));
      }
      return reversed;
    }

    /**
     * Return the opposite direction.
     *
     * @return opposite
    **/
    public static DirectionEnum getOppositeDirection(DirectionEnum dir) {
      switch(dir) {
        case LEFT:
          return DirectionEnum.RIGHT;
        case UP:
          return DirectionEnum.DOWN;
        case RIGHT:
          return DirectionEnum.LEFT;
        case DOWN:
          return DirectionEnum.UP;
      }
      return null;
    }

    public Stack<DirectionEnum> getSteps() {
      return this.steps;
    }
  }

  private HashMap<PathType, List<Path>> paths = new HashMap<>();

  private EventManager eventManager;

  public PathManager(@Autowired EventManager eventManager) {
    this.eventManager = eventManager;
    this.eventManager.register(this);
  }

  /**
   *  Track a path type.
  **/
  public void addPath(PathType type) {
    List<Path> typePaths = this.paths.get(type);
    if(typePaths == null) {
      typePaths = new ArrayList<Path>();
      this.paths.put(type, typePaths);
    }
    typePaths.add(new Path());
  }

  /**
   * Implements the event listener action.
   *
   * @return
  **/
  @Override
  public void doAction(Event event) {
    if(event instanceof MoveEvent) {
      this.updatePaths((MoveEvent) event);
    }
    if(event instanceof FoundExitEvent) {
      this.addPath(PathType.EXIT);
    }
    if(event instanceof FoundCollectEvent) {
      this.addPath(PathType.COLLECT);
    }
    if(event instanceof FoundStartEvent) {
      this.addPath(PathType.START);
    }
  }

  /**
   * Update the paths from some starting point to the current location.
   *
   * This means we only adjust for the end of the path. If we went back 
   * the opposite direction we simply delete the last step. Else we append
   * to it.
   *
   * @return
  **/
  public void updatePaths(MoveEvent event) {
    for(List<Path> typePaths: this.paths.values()) {
      for(Path path: typePaths) {
        if(path.steps.size() > 0) {
          DirectionEnum lastDir = path.steps.get(path.steps.size() -1);
          if(Path.getOppositeDirection(event.getDirection()).equals(lastDir)) {
            // If the opposite of the new move equals the last move of the step
            // then just pop the last element of the path, since they would
            // cancel each other out.
            path.steps.pop();
          } else {
            // Else we have an extension of the path.
            path.steps.add(event.getDirection());
          }
        } else {
          // Initial case where we just departed from the tracked tile.
          path.steps.add(event.getDirection());
        }
      }
    }
  }

  /**
   * Retrieve the shortest path to a path type (tile of interest).
   *
   * @return path
  **/
  public Path getNearestPath(PathType type) {
    List<Path> paths = this.paths.get(type);
    Comparator<Path> c = new Comparator<PathManager.Path>() {
      public int compare(Path a, Path b) {
        return a.steps.size() - b.steps.size();
      }
    };
    return paths.stream().min(c).get();
  }
}
