/**
 * Abstract move interaction class for when playing manually.
 * Child classes indicate the direction which to move to.
 *
 * Optimisation: Move the Down Left Right Up to be inner classes.
 * The reason they aren't right now is due to insufficient Spring
 * knowledge.
 *
 * @author Quoc An Ha (qa.ha@pm.me)
**/
package com.quocanha.hightechict.maze.client.interaction.implementations;

import java.util.Optional;

import com.quocanha.hightechict.maze.client.interaction.Interaction;
import com.quocanha.hightechict.maze.client.interaction.InteractionException;

import org.springframework.stereotype.Component;

import io.swagger.client.ApiException;
import io.swagger.client.model.MoveAction.DirectionEnum;

@Component
public abstract class MoveInteraction extends InteractionImpl {

  abstract DirectionEnum getDirection();

  public Optional<Interaction> run() throws InteractionException {
    try {
      this.engine.requestMove(this.getDirection().toString());
    } catch(ApiException e) {
      e.printStackTrace();
    }

    Interaction playInteraction = this.appContext.getBean(PlayInteraction.class);
    return Optional.of(playInteraction);
  }
}
